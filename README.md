# Digidoc

Digidoc est une interface graphique simple pour créer des documents sur un serveur Etherpad (https://github.com/ether/etherpad-lite).

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte Mona Sans Expanded (Sil Open Font Licence 1.1)

### Variables d'environnement (fichier .env à créer à la racine du dossier)
```
AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
ETHERPAD_SERVER (lien vers un serveur Etherpad)
ETHERPAD_API (clé d'API du serveur Etherpad)
```

### Serveur PHP nécessaire pour l'API
```
php -S localhost:8000 (pour le développement uniquement)
```

### Démo
https://ladigitale.dev/digidoc/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

