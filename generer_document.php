﻿<?php

session_start();

$env = '.env';
$donneesEnv = '';
if (isset($_SESSION['serveurEtherpad']) && $_SESSION['serveurEtherpad'] !== '') {
	$etherpad_server = $_SESSION['serveurEtherpad'];
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$etherpad_server = getenv('ETHERPAD_SERVER');
	$_SESSION['serveurEtherpad'] = $etherpad_server;
} else {
	$etherpad_server = '';
}

if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
	$domainesAutorises = $_SESSION['domainesAutorises'];
} else if ($donneesEnv !== '') {
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else if (file_exists($env)) {
	$donneesEnv = explode("\n", file_get_contents($env));
	foreach ($donneesEnv as $ligne) {
		preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
		if (isset($matches[2])) {
			putenv(trim($ligne));
		}
	}
	$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
	$_SESSION['domainesAutorises'] = $domainesAutorises;
} else {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	$domainesAutorises = '';
}

if ($domainesAutorises !== '') {
	if ($domainesAutorises === '*') {
		$origine = $domainesAutorises;
	} else {
		$domainesAutorises = explode(',', $domainesAutorises);
		$origine = $_SERVER['SERVER_NAME'];
	}
	if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
		header('Access-Control-Allow-Origin: $origine');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	} else {
		header('Location: /');
		exit();
	}
}

if (!empty($_POST['creation'])) {
	$id = uniqid('', false);
	echo $etherpad_server . '/p/' . $id;
	exit();
} else {
	header('Location: /');
}

?>
